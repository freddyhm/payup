//
//  NewUserViewController.h
//  payup
//
//  Created by Freddy Hidalgo-Monchez on 12-07-22.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface NewUserViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnDone;

@property (nonatomic, weak) IBOutlet UITextField *txtUserName;
@property (nonatomic, weak) IBOutlet UITextField *txtPass;
@property (nonatomic, weak) IBOutlet UITextField *txtPassAgain;

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;
- (void)createPlayer:(PFUser *)user;


@end
