//
//  NewUserViewController.m
//  payup
//
//  Created by Freddy Hidalgo-Monchez on 12-07-22.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NewUserViewController.h"

#import "ConnectViewController.h"

@interface NewUserViewController ()

- (void)processEntries;
- (void)txtInputChanged:(NSNotification *)note;
- (BOOL)enableDoneBtn;

@end

@implementation NewUserViewController

@synthesize btnDone;
@synthesize txtUserName;
@synthesize txtPass, txtPassAgain;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

# pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // setup the notifications - FHM    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(txtInputChanged:) name:UITextFieldTextDidChangeNotification object:txtUserName];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(txtInputChanged:) name:UITextFieldTextDidChangeNotification object:txtPass];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(txtInputChanged:) name:UITextFieldTextDidChangeNotification object:txtPassAgain];
    
    btnDone.enabled = NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    // remove notifications when app low on memory - FHM
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:txtUserName];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:txtPass];
    
    [[NSNotificationCenter  defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:txtPassAgain];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)viewWillAppear:(BOOL)animated
{
    // username textfield is selected - FHM
    
    [txtUserName becomeFirstResponder];
    [super viewWillAppear:animated];
}

- (void)dealloc
{
    // remove notifications when view gets deleted - FHM
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:txtUserName];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:txtPass];
    
    [[NSNotificationCenter  defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:txtPassAgain];
}

# pragma mark - TextFieldDelegate methods

// called when return button is touched on keyboard - FHM
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    // highlights next field depending on what textfield user is on - FHM
    
    if(textField == txtUserName)
    {
        [txtPass becomeFirstResponder];
    }
    
    if(textField == txtPass)
    {
        [txtPassAgain becomeFirstResponder];
    }
    
    if(textField == txtPassAgain)
    {
        [txtPassAgain resignFirstResponder];
        [self processEntries];
    }
    
    return YES;
}

#pragma mark - UITextfield text field change notifications

// checks if textfields are empty or not, enable done button - FHM
- (BOOL)enableDoneBtn
{
    BOOL enableDoneBtn = NO;
    
    
    if(txtUserName.text != nil && txtUserName.text.length > 0 &&
       txtPass.text != nil && txtPassAgain.text.length > 0 &&
       txtPassAgain.text != nil && txtPassAgain.text.length > 0)
    {
        enableDoneBtn = YES;
        
    }
    
    return enableDoneBtn;
}

// fired when input has changed in textfield - FHM
- (void) txtInputChanged:(NSNotification *)note
{
    btnDone.enabled = [self enableDoneBtn];
}

#pragma mark - IBActions

// dismiss modal view when touched - FHM
- (IBAction)cancel:(id)sender
{
    [self.presentingViewController dismissModalViewControllerAnimated:YES];
}

// dismiss keyboard and process input - FHM
- (IBAction)done:(id)sender
{
    [txtUserName resignFirstResponder];
    [txtPass resignFirstResponder];
    [txtPassAgain resignFirstResponder];
    [self processEntries];
}

#pragma mark - Private Methods
#pragma mark Field Validation

- (void)processEntries
{
    // init var for error msg
    NSString *username = txtUserName.text;
    NSString *pass = txtPass.text;
    NSString *passAgain = txtPassAgain.text;
    NSString *errorMsg = @"Please ";
    NSString *usernameBlank = @"enter a username";
    NSString *passBlank = @"enter a password";
    NSString *joinText = @". and ";
    NSString *passMismatch = @"enter the same password twice";

    
    BOOL textError = NO;
    
    // build error message depending on case - FHM
    if(username.length == 0 || pass.length == 0 || passAgain.length == 0)
    {
        if (passAgain.length == 0) {
            [txtPassAgain becomeFirstResponder];
        }
        
        if (pass.length == 0) {
            [txtPass becomeFirstResponder];
        }
    
        if (username.length == 0) {
            [txtUserName becomeFirstResponder];
            errorMsg = [errorMsg stringByAppendingString:usernameBlank];
        }
    
        if (pass.length == 0 || passAgain.length == 0) {
            if (username.length == 0) {
                errorMsg = [errorMsg stringByAppendingString:joinText];
            }
            errorMsg = [errorMsg stringByAppendingString:passBlank];
        }
        goto showDialog;
        
    }

    // compare inputted passwords - FHM
    if ([pass compare:passAgain] != NSOrderedSame) {
        textError = YES;
        errorMsg = [errorMsg stringByAppendingString:passMismatch];
        [txtPass becomeFirstResponder];
        goto showDialog;
    }
    
//shows error message - FHM
showDialog:
    if(textError){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:errorMsg message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
        return;
    }
    
    btnDone.enabled = NO;

    // add connecting view - FHM
    ConnectViewController *connectView = [self.storyboard instantiateViewControllerWithIdentifier:@"ConnectViewController"];    
    [self.view addSubview:connectView.view];


    PFUser *user = [PFUser user];
    user.username = username;
    user.password = pass;
    
    // complete sign up - FHM
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        // remove the connect view from stack - FHM
        [connectView.view removeFromSuperview];
        
        if(error)
        {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[[error userInfo] objectForKey:@"error"] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            [alertView show];
            btnDone.enabled = [self enableDoneBtn];
            
            // Bring the keyboad back up - FHM
            [txtUserName becomeFirstResponder];
        }
        else 
        {
            
            [self createPlayer:user];
            
            // manually perform the segue passing nothing - FHM
            [self performSegueWithIdentifier:@"NewUserTabBarController" sender:nil];
        }
    }];
}

// create player obj and link user to player - FHM
-(void)createPlayer:(PFUser *)user
{
    PFObject *player = [PFObject objectWithClassName:@"Player"];
    [player setObject:[NSNumber numberWithLongLong:0] forKey:@"fb_id"];
    [player setObject:[NSNumber numberWithInt:0] forKey:@"pay"];
    [player setObject:[NSNumber numberWithInt:0] forKey:@"safe"];
    [player setObject:[NSNumber numberWithInt:0]  forKey:@"total"];
    [player setObject:@"" forKey:@"pic_big"];
    [player setObject:@"" forKey:@"pic_small"];
    [player setObject:user forKey:@"user"];
    [player saveInBackground];    
}





@end
