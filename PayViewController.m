//
//  PayViewController.m
//  payup
//
//  Created by Freddy Hidalgo-Monchez on 12-07-22.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PayViewController.h"
#import <Parse/Parse.h>

@interface PayViewController ()

@end

@implementation PayViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - View Life Cycle
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark - Facebook Request Methods
// Makes a request and logs out if session is invalid - FHM
-(void)request:(PF_FBRequest *)request didFailWithError:(NSError *)error {
    
    // OAuthException means our session is invalid - FHM
    if ([[[[error userInfo] objectForKey:@"error"] objectForKey:@"type"] 
         isEqualToString: @"OAuthException"]) {
        NSLog(@"The facebook token was invalidated");
        // Log out user - FHM
        [self logoutButtonTouchHandler:nil]; 
    } else {
        NSLog(@"Some other error");
    }
}

#pragma mark - User Management Methods
// Log out the user - FHM
- (IBAction)logoutButtonTouchHandler:(id)sender 
{
    [PFUser logOut]; 
    
    // Return to login page
    [self.navigationController popToRootViewControllerAnimated:YES]; 
}

@end
