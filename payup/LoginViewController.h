//
//  LoginViewController.h
//  payup
//
//  Created by Freddy Hidalgo-Monchez on 12-07-22.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *btnDone;

@property (nonatomic, weak) IBOutlet UITextField *txtUserName;
@property (nonatomic, weak) IBOutlet UITextField *txtPass;

- (IBAction)cancel:(id)sender;
- (IBAction)done:(id)sender;

@end
