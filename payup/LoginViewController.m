//
//  LoginViewController.m
//  payup
//
//  Created by Freddy Hidalgo-Monchez on 12-07-22.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LoginViewController.h"

#import <Parse/Parse.h>
#import "ConnectViewController.h"


@interface LoginViewController ()

- (void)processEntries;
- (void)txtInputChanged:(NSNotification *)note;
- (BOOL)enableDoneBtn;

@end

@implementation LoginViewController

@synthesize btnDone;
@synthesize txtUserName;
@synthesize txtPass;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark - View Life Cycle


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(txtInputChanged:) name:UITextFieldTextDidChangeNotification object:txtUserName];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(txtInputChanged:) name:UITextFieldTextDidChangeNotification object:txtPass];
        
    btnDone.enabled = NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    // remove notifications when app low on memory - FHM
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:txtUserName];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:txtPass];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    // username textfield is selected - FHM
    
    [txtUserName becomeFirstResponder];
    [super viewWillAppear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc
{
    // remove notifications when view gets deleted - FHM
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:txtUserName];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:txtPass];
    
}

#pragma mark - UITextfield text field change notifications

// checks if textfields are empty or not, enable done button - FHM
- (BOOL)enableDoneBtn
{
    BOOL enableDoneBtn = NO;
    
    
    if(txtUserName.text != nil && txtUserName.text.length > 0 &&
       txtPass.text != nil)
    {
        enableDoneBtn = YES;
        
    }
    
    return enableDoneBtn;
}

// fired when input has changed in textfield - FHM
- (void) txtInputChanged:(NSNotification *)note
{
    btnDone.enabled = [self enableDoneBtn];
    
}
# pragma mark - TextFieldDelegate methods
    
// called when return button is touched on keyboard - FHM
- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    // highlights next field depending on what textfield user is on - FHM
    
    if(textField == txtUserName)
    {
        [txtPass becomeFirstResponder];
    }
    
    if(textField == txtPass)
    {
        [txtPass resignFirstResponder];
        [self processEntries];
    }
    
    return YES;
}

#pragma mark - IBActions

// dismiss modal view when touched - FHM
- (IBAction)cancel:(id)sender
{
    [self.presentingViewController dismissModalViewControllerAnimated:YES];
}

// dismiss keyboard and process input - FHM
- (IBAction)done:(id)sender
{
    [txtUserName resignFirstResponder];
    [txtPass resignFirstResponder];
    [self processEntries];
}

#pragma mark Field Validation

- (void)processEntries
{
    // init var for error msg
    NSString *username = txtUserName.text;
    NSString *pass = txtPass.text;
    NSString *noUsername = @"username";
    NSString *noPass = @"password";
    NSString *errorText = @"No ";
    NSString *errorTextJoin = @" or ";
    NSString *errorTextEnding = @" entered";
    
    BOOL textError = NO;
    
    // build error message depending on case - FHM
    if(username.length == 0 || pass.length == 0)
    {
        textError = YES;
        
        if (username.length == 0) {
            [txtPass becomeFirstResponder];
        }
        
        if (pass.length == 0){
            [txtUserName becomeFirstResponder];
        }
    }
    
    if (username.length == 0) {
        textError = YES;
        errorText = [errorText stringByAppendingString:noUsername];
    }
    
    if (pass.length == 0) {
        textError = YES;
        
        if(username.length == 0){
            errorText = [errorText stringByAppendingString:errorTextJoin];
        }
        errorText = [errorText stringByAppendingString:noPass];
    }
    
    if(textError){
        errorText = [errorText stringByAppendingString:errorTextEnding];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:errorText message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alertView show];
        return;
    }
    
    btnDone.enabled = NO;
    
    // add connecting view - FHM
    ConnectViewController *connectViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ConnectViewController"];    
    [self.view addSubview:connectViewController.view];
    
    // complete sign up - FHM
    [PFUser logInWithUsernameInBackground:username password:pass block:^(PFUser *user, NSError *error) {
        
        //remove the connect view from stack - FHM
        [connectViewController.view removeFromSuperview];
        
        // push my pay view if user exists
        if(user){
            // manually perform the segue passing nothing - FHM
            [self performSegueWithIdentifier:@"LoginTabBarController" sender:nil];
        } 
        else {
            NSLog(@"%s didn't get a user!", __PRETTY_FUNCTION__);
            
            btnDone.enabled = [self enableDoneBtn];
            UIAlertView *alert = nil;
            
            if (error == nil)
            {
                alert = [[UIAlertView alloc] initWithTitle:@"Couldn't log in:\nThe username or password were wrong." message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                
            } else {
                alert = [[UIAlertView alloc] initWithTitle:[[error userInfo] objectForKey:@"error"] message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
            }
            [alert show];
            
            // Bring the keyboad back up - FHM
            [txtUserName becomeFirstResponder];
        }
    }];
}



@end
