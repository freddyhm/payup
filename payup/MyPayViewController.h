//
//  MyPayViewController.h
//  payup
//
//  Created by Freddy Hidalgo-Monchez on 12-07-24.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>

@interface MyPayViewController : UIViewController <PF_FBRequestDelegate, NSURLConnectionDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imgViewProfilePic;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblStats;


@end
