//
//  MyPayViewController.m
//  payup
//
//  Created by Freddy Hidalgo-Monchez on 12-07-24.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MyPayViewController.h"

@interface MyPayViewController ()

-(void)loadProfile;

@end

@implementation MyPayViewController
{
    NSMutableData *imageData;
    NSDictionary *fbData;
}

@synthesize imgViewProfilePic;
@synthesize lblName;
@synthesize lblStats;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


#pragma mark - View Life Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    
    NSMutableDictionary * params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    @"SELECT uid, name, pic_big, pic_small FROM user WHERE uid = me()", @"query",
                                    nil];

    // Send request to facebook
    [[PFFacebookUtils facebook]requestWithMethodName: @"fql.query"
                                           andParams: params
                                       andHttpMethod: @"POST"
                                         andDelegate: self];
}

- (void)viewDidUnload
{
    [self setImgViewProfilePic:nil];
    [self setLblName:nil];
    [self setLblStats:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Custom Methods

-(void)loadProfile{
    
    PFUser *currentUser = [PFUser currentUser];
    
    // check if fb data is set - FHM
    if(fbData != nil)
    {
        // check if username is set  (logged)
        if([currentUser username] == nil)
        {
            currentUser.username = [fbData objectForKey:@"name"];
            currentUser.fb_id = [fbData objectForKey:@"uid"];
            currentUser.pic_small = [fbData objectForKey:@"pic_small"]; 
            currentUser.pic_big = [fbData objectForKey:@"pic_big"]; 
            [currentUser saveInBackground];
        }
        
        lblName.text = [fbData objectForKey:@"name"];
    }
    else {
        lblName.text = [currentUser objectForKey:@"username"];
    }
    
}


#pragma mark - Facebook Request Delegate methods

-(void)request:(PF_FBRequest *)request didLoad:(id)result {
    
    // Typecast result in array and then in dictionary - FHM
    NSArray *data = (NSArray *)result;
    fbData = [data objectAtIndex:0];

    // Load image in image view - FHM
    imageData = [[NSMutableData alloc] init]; 
    
    // Get pic url from dictionary - FHM
    NSString *pictureURL = [fbData objectForKey:@"pic_big"];
  
    // Create URLRequest - FHM
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:pictureURL]cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:2];
    
    // Run request asynchronously - FHM
    NSURLConnection *urlConnection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self];
    
    [self loadProfile];
}

#pragma mark - User Management Methods
// Log out the user - FHM
- (IBAction)logoutButtonTouchHandler:(id)sender 
{
    [PFUser logOut]; 
    
    // Return to login page - FHM
    [self.navigationController popToRootViewControllerAnimated:YES]; 
}

#pragma mark - NSURLConnectionDelegate methods

// Called every time a chunk of the pic data is received - FHM
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    
    [imageData appendData:data]; // Build the image
}

// Called when the entire image is finished downloading - FHM
-(void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    // Set the image in the header imageView
    [imgViewProfilePic setImage:[UIImage imageWithData:imageData]];
}

@end